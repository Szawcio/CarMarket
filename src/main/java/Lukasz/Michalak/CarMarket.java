package Lukasz.Michalak;


import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import static java.util.Comparator.comparing;

public class CarMarket {


    private static int announcementNumber = 1;
    private List<Car> carsList;
    private List<Truck> trucksList;


    public CarMarket() {
        carsList = new ArrayList<>();
        trucksList = new ArrayList<>();
    }


    public String typeOfVehicle() {
        String type;
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose type of vehicle");
        type = sc.nextLine();
        return type;
    }

    public String marqueOfVehicle() {
        String marque;
        Scanner sc = new Scanner(System.in);
        System.out.println("What is the marque of vehicle");
        marque = sc.nextLine();
        return marque;
    }

    public String modelOfVehicle() {
        String model;
        Scanner sc = new Scanner(System.in);
        System.out.println("What is the model of vehicle");
        model = sc.nextLine();
        return model;
    }

    public String colourOfVehicle() {
        String colour;
        Scanner sc = new Scanner(System.in);
        System.out.println("What is the colour of vehicle");
        colour = sc.nextLine();
        return colour;
    }

    public LocalDate productionYearOfVehicle() {
        String date;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter date of vehicle production");
        date = sc.nextLine();
        LocalDate localDate = new LocalDate(LocalDate.parse(date));
        return localDate;
    }

    public int mileAgeOfVehicle() {
        int mileAge;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter mileage");
        mileAge = sc.nextInt();
        return mileAge;
    }

    public int horsePowerOfVehicle() {
        int horsepower;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter amount of horse power");
        horsepower = sc.nextInt();
        return horsepower;
    }

    public boolean ifFirstOwner() {
        boolean fOwner;
        String input;
        Scanner sc = new Scanner(System.in);
        System.out.println("Is it vehicle's first owner?");
        input = sc.nextLine();
        if (input.equals("yes")) {
            return fOwner = true;
        } else {
            return fOwner = false;
        }
    }

    public int priceOfVehicle() {
        int price;
        Scanner sc = new Scanner(System.in);
        System.out.println("What is the price of the vehicle?");
        price = sc.nextInt();
        return price;
    }

    public int loadOfVehicle() {
        int load;
        Scanner sc = new Scanner(System.in);
        System.out.println("What is vehile's load");
        load = sc.nextInt();
        return load;
    }


    public void addVehicleToList() {
        String type = typeOfVehicle();
        if (type.equals("car") || type.equals("truck")) {
            String tempMarque = marqueOfVehicle();
            String tempModel = modelOfVehicle();
            String tempColour = colourOfVehicle();
            LocalDate tempYear = productionYearOfVehicle();
            int tempMileAge = mileAgeOfVehicle();
            int tempHorsePower = horsePowerOfVehicle();
            boolean tempFirstOwner = ifFirstOwner();
            int tempPrice = priceOfVehicle();
            if (type.equals("car")) {
                Car car = new Car.Builder()
                        .marque(tempMarque)
                        .model(tempModel)
                        .colour(tempColour)
                        .productionDate(tempYear)
                        .mileAge(tempMileAge)
                        .horsePower(tempHorsePower)
                        .firstOwner(tempFirstOwner)
                        .price(tempPrice)
                        .id(announcementNumber++)
                        .build();
                getCarsList().add(car);
                System.out.println("Car announcement added correctly");
            }
            if (type.equals("truck")) {
                int tempLoad = loadOfVehicle();
                Truck truck = new Truck.Builder()
                        .marque(tempMarque)
                        .model(tempModel)
                        .colour(tempColour)
                        .productionDate(tempYear)
                        .mileAge(tempMileAge)
                        .horsePower(tempHorsePower)
                        .firstOwner(tempFirstOwner)
                        .price(tempPrice)
                        .loadInKg(tempLoad)
                        .id(announcementNumber++)
                        .build();
                getTrucksList().add(truck);
                System.out.println("Truck announcement added correctly");
            }
        } else {
            System.out.println("Incorrect type of vehicle");
        }
    }

    public void deleteAnnouncement() {
        Scanner sc = new Scanner(System.in);
        int id = sc.nextInt();
        getCarsList().removeIf(car -> id == car.getId());
        getTrucksList().removeIf(truck -> id == truck.getId());
    }

    public void search() {
        boolean endOfSearching = false;
        Scanner sc = new Scanner(System.in);
        List<Car> tempCars = new ArrayList<>(getCarsList());
        List<Truck> tempTrucks = new ArrayList<>(getTrucksList());
        System.out.println("Would u like to search for a car/truck");
        String answer = sc.nextLine();
        if (answer.equals("car")) {
            System.out.println("All cars offer: ");
            printSortedCarsById(getCarsList());
            System.out.println("Specify offer!");
            while (!endOfSearching) {
                System.out.println("Choose: price, marque, model, colour, production date, mileage, horsepower,end(of searching)");
                String searchParamether = sc.nextLine();
                switch (searchParamether) {
                    case "model":
                        String tempModel = modelOfVehicle();
                        tempCars.removeIf(car -> !tempModel.equals(car.getModel()));
                        printSortedCarsById(tempCars);
                        endOfSearching = false;
                        break;
                    case "marque":
                        String tempMarque = marqueOfVehicle();
                        tempCars.removeIf(car -> !tempMarque.equals(car.getMarque()));
                        printSortedCarsById(tempCars);
                        endOfSearching = false;
                        break;
                    case "price":
                        System.out.println("You will enter top bound of price");
                        int tempPrice = priceOfVehicle();
                        tempCars.removeIf(car -> tempPrice < car.getPrice());
                        printSortedCarsById(tempCars);
                        endOfSearching = false;
                        break;
                    case "colour":
                        String tempColour = colourOfVehicle();
                        tempCars.removeIf(car -> !tempColour.equals(car.getColour()));
                        printSortedCarsById(tempCars);
                        endOfSearching = false;
                        break;
                    case "production date":
                        System.out.println("You will enter date, which will be date for the oldest vehicle you wanna see");
                        LocalDate tempLocalDate = productionYearOfVehicle();
                        tempCars.removeIf(car -> tempLocalDate.isBefore(car.getProductionDate()));
                        printSortedCarsById(tempCars);
                        endOfSearching = false;
                        break;
                    case "mileage":
                        System.out.println("You will enter top bound of mileage");
                        int tempMileAge = mileAgeOfVehicle();
                        tempCars.removeIf(car -> tempMileAge < car.getMileAge());
                        printSortedCarsById(tempCars);
                        endOfSearching = false;
                        break;
                    case "horsepower":
                        System.out.println("You will enter top bound of horsepower");
                        int tempHorsePower = horsePowerOfVehicle();
                        tempCars.removeIf(car -> tempHorsePower < car.getHorsePower());
                        printSortedCarsById(tempCars);
                        endOfSearching = false;
                        break;
                    case "end":
                        endOfSearching = true;
                        break;
                    default:
                        System.out.println("Paramether unknown");
                        endOfSearching = false;
                        break;
                }
            }
        } else if (answer.equals("truck")) {
            System.out.println("All trucks offer: ");
            printSortedTrucksById(getTrucksList());
            System.out.println("Specify offer!");
            while (!endOfSearching) {
                System.out.println("Choose: price, marque, model, colour, production date, mileage, horsepower, load, end(of searching)");
                String searchParamether = sc.nextLine();
                switch (searchParamether) {
                    case "model":
                        String tempModel = modelOfVehicle();
                        tempTrucks.removeIf(truck -> !tempModel.equals(truck.getModel()));
                        printSortedTrucksById(tempTrucks);
                        endOfSearching = false;
                        break;
                    case "marque":
                        String tempMarque = marqueOfVehicle();
                        tempTrucks.removeIf(truck -> !tempMarque.equals(truck.getMarque()));
                        printSortedTrucksById(tempTrucks);
                        endOfSearching = false;
                        break;
                    case "price":
                        System.out.println("You will enter top bound of price");
                        int tempPrice = priceOfVehicle();
                        tempTrucks.removeIf(truck -> tempPrice < truck.getPrice());
                        printSortedTrucksById(tempTrucks);
                        endOfSearching = false;
                        break;
                    case "colour":
                        String tempColour = colourOfVehicle();
                        tempTrucks.removeIf(truck -> !tempColour.equals(truck.getColour()));
                        printSortedTrucksById(tempTrucks);
                        endOfSearching = false;
                        break;
                    case "production date":
                        System.out.println("You will enter date, which will be date for the oldest vehicle you wanna see");
                        LocalDate tempLocalDate = productionYearOfVehicle();
                        tempTrucks.removeIf(truck -> tempLocalDate.isBefore(truck.getProductionDate()));
                        printSortedTrucksById(tempTrucks);
                        endOfSearching = false;
                        break;
                    case "mileage":
                        System.out.println("You will enter top bound of mileage");
                        int tempMileAge = mileAgeOfVehicle();
                        tempTrucks.removeIf(truck -> tempMileAge < truck.getMileAge());
                        printSortedTrucksById(tempTrucks);
                        endOfSearching = false;
                        break;
                    case "horsepower":
                        System.out.println("You will enter top bound of horsepower");
                        int tempHorsePower = horsePowerOfVehicle();
                        tempTrucks.removeIf(truck -> tempHorsePower < truck.getHorsePower());
                        printSortedTrucksById(tempTrucks);
                        endOfSearching = false;
                        break;
                    case "load":
                        System.out.println("You will enter bottom bound of load in kg");
                        int tempLoad = loadOfVehicle();
                        tempTrucks.removeIf(truck -> tempLoad > truck.getLoadInKg());
                        printSortedTrucksById(tempTrucks);
                        endOfSearching = false;
                        break;
                    case "end":
                        endOfSearching = true;
                        break;
                    default:
                        System.out.println("Paramether unknown");
                        endOfSearching = false;
                        break;
                }
            }
        } else {
            System.out.println("Inncorrent type of vehicle");
        }
    }

    public void printSortedCarsById(List<Car> list) {
        List<Car> tempList = new ArrayList<>(list);
        tempList.sort(comparing(Car::getId));
        for (Car car : tempList) {
            System.out.println(car.toString());
        }
    }

    public void printSortedTrucksById(List<Truck> list) {
        List<Truck> tempList = new ArrayList<>(list);
        tempList.sort(comparing(Truck::getId));
        for (Truck truck : tempList) {
            System.out.println(truck.toString());
        }
    }

    public void manageCarMarket() {
        Scanner sc = new Scanner(System.in);
        boolean ifEnd = false;
        while (!ifEnd) {
            System.out.println("What would you like to do:");
            System.out.println("Options:");
            System.out.printf("1.Add announcement(add)" + "\n" + "2.Remove announcement(del)" + "\n" + "3.Search vehicle(search)" + "\n" + "4.End(end)");
            String userInput = sc.nextLine();
            if (userInput.equals("add")) {
                addVehicleToList();
                ifEnd = false;
            } else if (userInput.equals("del")) {
                System.out.println("Enter id for vehicle");
                deleteAnnouncement();
                ifEnd = false;
            } else if (userInput.equals("search")) {
                search();
                ifEnd = false;
            } else if (userInput.equals("end")) {
                ifEnd = true;
            } else {
                System.out.println("Order unknown");
                ifEnd = false;
            }

        }


    }


    public List<Car> getCarsList() {
        return carsList;
    }

    public void setCarsList(List<Car> carsList) {
        this.carsList = carsList;
    }

    public List<Truck> getTrucksList() {
        return trucksList;
    }

    public void setTrucksList(List<Truck> trucksList) {
        this.trucksList = trucksList;
    }
}
