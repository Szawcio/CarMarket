package Lukasz.Michalak;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        CarMarket carMarket = new CarMarket();
        //carMarket.addVehicleToList();

        Car car0 = new Car("opel","vectra","red",new LocalDate("2011"),10000,100,true,2000,4);
        System.out.println(car0.toString());

        Truck truck=new Truck("mercedes","vito","red",new LocalDate("2011"),10000,100,true,2500,2000,4);
        System.out.println(truck.toString());
        System.out.println("");


        Car car1 = new Car("opel","vectra","red",new LocalDate("2011"),10000,100,true,2000,4);
        Car car2 = new Car("opel","vectra","red",new LocalDate("2000"),9000,112,true,1000,2);
        Car car3 = new Car("opel","vectra","red",new LocalDate("1991"),15000,101,true,100,1);
        Car car4 = new Car("opel","vectra","red",new LocalDate("2003"),20000,109,true,800,3);

        carMarket.getCarsList().add(car1);
        carMarket.getCarsList().add(car2);
        carMarket.getCarsList().add(car3);
        carMarket.getCarsList().add(car4);

        carMarket.printSortedCarsById(carMarket.getCarsList());
        carMarket.manageCarMarket();









    }

/*    public List<Car> costam(List<Car> carList) {
        return carList.parallelStream()
                .filter(f -> f.getPrice() < 500)
                .sorted(comparing(Car::getPrice))
                .collect(toList());
    }*/
}
