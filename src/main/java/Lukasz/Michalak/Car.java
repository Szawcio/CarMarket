package Lukasz.Michalak;


import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

public class Car {
    private String marque;
    private String model;
    private String colour;
    private LocalDate productionDate;
    private int mileAge;
    private int horsePower;
    private boolean firstOwner;
    private int price;
    private int id;

    public Car(String marque, String model, String colour, LocalDate productionDate, int mileAge, int horsePower, boolean firstOwner, int price, int id) {
        this.marque = marque;
        this.model = model;
        this.colour = colour;
        this.productionDate = productionDate;
        this.mileAge = mileAge;
        this.horsePower = horsePower;
        this.firstOwner = firstOwner;
        this.price = price;
        this.id = id;

    }

    public Car(Builder builder) {
        this.marque = builder.marque;
        this.model = builder.model;
        this.colour = builder.colour;
        this.productionDate = builder.productionDate;
        this.mileAge = builder.mileAge;
        this.horsePower = builder.horsePower;
        this.firstOwner = builder.firstOwner;
        this.price = builder.price;
        this.id = builder.id;

    }

    @Override
    public String toString() {
        String answer;
        if (firstOwner) {
            answer = "yes";
        } else {
            answer = "no";
        }
        return "(" + id + ")" + " Car: " + marque + " " + model + ", Colour: " + colour + ", Produced in: " + productionDate.getYear()
                + ", Mileage: " + mileAge + " km, " + " Horsepower: " + horsePower + ", First owner: " + answer + ", Price: " + price;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(LocalDate productionDate) {
        this.productionDate = productionDate;
    }

    public int getMileAge() {
        return mileAge;
    }

    public void setMileAge(int mileAge) {
        this.mileAge = mileAge;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public boolean isFirstOwner() {
        return firstOwner;
    }

    public void setFirstOwner(boolean firstOwner) {
        this.firstOwner = firstOwner;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static class Builder {
        private String marque;
        private String model;
        private String colour;
        private LocalDate productionDate;
        private int mileAge;
        private int horsePower;
        private boolean firstOwner;
        private int price;
        private int id;

        public Builder marque(String marque) {
            this.marque = marque;
            return this;
        }

        public Builder model(String model) {
            this.model = model;
            return this;
        }

        public Builder colour(String colour) {
            this.colour = colour;
            return this;
        }

        public Builder productionDate(LocalDate productionDate) {
            this.productionDate = productionDate;
            return this;
        }

        public Builder mileAge(int mileAge) {
            this.mileAge = mileAge;
            return this;
        }

        public Builder horsePower(int horsePower) {
            this.horsePower = horsePower;
            return this;
        }

        public Builder firstOwner(boolean firstOwner) {
            this.firstOwner = firstOwner;
            return this;
        }

        public Builder price(int price) {
            this.price = price;
            return this;
        }

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Car build() {
            return new Car(this);
        }


    }
}

