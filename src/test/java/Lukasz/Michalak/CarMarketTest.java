package Lukasz.Michalak;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Łukasz on 2017-08-02.
 */
public class CarMarketTest {

    private CarMarket carMarket;
    private Car car1;
    private Car car2;
    private Car car3;
    private Car car4;

    @Before
    public void setup(){
        carMarket=new CarMarket();
        car1 = new Car("opel","vectra","red",new LocalDate(2011),10000,100,true,2000,1);
        car2 = new Car("opel","vectra","red",new LocalDate(2011),10000,100,true,2000,2);
        car3 = new Car("opel","vectra","red",new LocalDate(2011),10000,100,true,2000,3);
        car4 = new Car("opel","vectra","red",new LocalDate(2011),10000,100,true,2000,4);

    }

    @Test
    public void testAddVehicleToList(){
        carMarket.addVehicleToList();
        boolean result =carMarket.getCarsList().get(0).getModel().equals("opel");
        assertTrue(result);

    }

}
